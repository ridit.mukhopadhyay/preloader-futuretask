package com.ridit.java.FutureTaskAndExecutorService;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<ProductInfo> list = new ArrayList<ProductInfo>();
		PreLoader preLoader = new PreLoader();
		
		preLoader.startTheTask();
		for(int i = 0; i < 10; i++) {
			System.out.println("Main is working on other works...");
		}
		
		
		while(!preLoader.isDone()) {
			System.out.println("Background task is yet to be finished");
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		list = preLoader.get();
		
		System.out.println("Name : " + list.get(0).getName());
		
		PreLoader.executor.shutdown();

	}

}
