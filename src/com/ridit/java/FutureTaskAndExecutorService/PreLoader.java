package com.ridit.java.FutureTaskAndExecutorService;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

public class PreLoader {
	
	static ExecutorService executor = Executors.newFixedThreadPool(1);
	List<ProductInfo> productInfoList = new ArrayList<ProductInfo>();
	BackgroundLoadingTask task = new BackgroundLoadingTask();
	FutureTask<List<ProductInfo>> future = null;
	
	public void startTheTask() {
		future = (FutureTask<List<ProductInfo>>) executor.submit(task);
	}
	
	public boolean isDone() {
		return future.isDone();
	}
	
	public boolean cancel() {
		return future.cancel(true);
	}
	
	public List<ProductInfo> get(){
		try {
			productInfoList = future.get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return productInfoList;
	}

}
