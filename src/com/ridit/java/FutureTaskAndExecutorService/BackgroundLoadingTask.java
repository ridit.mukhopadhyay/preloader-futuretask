package com.ridit.java.FutureTaskAndExecutorService;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class BackgroundLoadingTask implements Callable<List<ProductInfo>> {
	List<ProductInfo> infoList = new ArrayList<ProductInfo>();
	@Override
	public List<ProductInfo> call() throws Exception {
		// TODO Auto-generated method stub
		System.out.println("Starting the task");
		for(int i = 0;i<10;i++) {
			infoList.add(new ProductInfo("MyProduct", 100.60));
			Thread.sleep(100);
		}
		
		return infoList;
	}

}
